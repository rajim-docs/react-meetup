import {useContext} from 'react';
import classes from './MeetupItem.module.css';
import Card from "../ui/Card";
import FavoritesContext from "../../store/favorites-context";

function MeetUpItem(props) {
    const favoriteContext = useContext(FavoritesContext);

    const itemIsFavorite = favoriteContext.itemIsFavorite(props.id);

    function toggleFavoritesStatusHandler() {
        if(itemIsFavorite) {
            favoriteContext.removeFavorite(props.id)
        } else {
            favoriteContext.addFavorite({
                id: props.id,
                title: props.title,
                description: props.description,
                image: props.image,
                address: props.address
            })
        }
    }

    return (
        <Card>
            <li className={classes.item}>
                <Card>
                    <div className={classes.image}>
                        <img src={props.image} alt={props.title}/>
                    </div>
                    <div className={classes.content}>
                        <h3>{props.title}</h3>
                        <address>{props.address}</address>
                        <p>{props.description}</p>
                    </div>
                    <div className={classes.actions}>
                        <button onClick={toggleFavoritesStatusHandler}>
                            {
                                itemIsFavorite ? 'Remove from Favorites':
                                    'Add to Favorites'
                            }
                        </button>
                    </div>
                </Card>
            </li>
        </Card>
    )
};

export default MeetUpItem;