import {useContext} from "react";
import FavoritesContext from "../store/favorites-context";
import MeetUpList from "../components/meetups/MeetUpList";

function FavoritesPage() {
    const favoriteContext = useContext(FavoritesContext);

    let content;

    if(favoriteContext.totalFavorites === 0) {
        content = <p>You got no favorites yet. Start adding some.</p>
    } else {
        content = <MeetUpList meetups={favoriteContext.favorites}/>
    }

    return (
        <section>
            <h1>My Favorites</h1>
            {content}
        </section>
    );
}

export default FavoritesPage;